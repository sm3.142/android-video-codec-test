package ch.felfel.videotest

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.MediaController
import android.widget.TextView
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import wseemann.media.FFmpegMediaMetadataRetriever
import java.io.File


class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    lateinit var videoView: VideoView
    lateinit var videoInfo: TextView
    lateinit var nextVideo: Button

    val videos = arrayOf("caminandes_llamigos_1080p.mp4", "caminandes_llamigos_1080p_hevc.mp4", "caminandes_llamigos_1080p.webm")

    var currentVideo = 0

    fun playVideo(name: String) {
        try {
            if (videoView.isPlaying()) {
                videoView.stopPlayback()
            }

            val file = File(this.getExternalFilesDir(""), name)
            Log.i(TAG, "file path ${file}")
            val uri = Uri.fromFile(file)

            val mmr = FFmpegMediaMetadataRetriever()
            mmr.setDataSource(this, uri)
            val md = mmr.metadata

            val vc = md.getString("video_codec")
            val ac = md.getString("audio_codec")
            val fr = md.getInt("framerate")
            val w = md.getInt("video_width")
            val h = md.getInt("video_height")

            videoInfo.text = "file=${file.name} codec=${vc}/${ac} framerate=${fr} size=${w}x${h}"

            videoView.setVideoURI(uri)
            videoView.requestFocus()
            videoView.start()
        } catch (t: Throwable) {
            Log.wtf(TAG,"failed to play video: ${t.localizedMessage}", t)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        videoView = findViewById(R.id.video_view)
        videoInfo = findViewById(R.id.video_info)
        nextVideo = findViewById(R.id.next_video)

        nextVideo.setOnClickListener() {
            currentVideo = ++currentVideo % 3

            playVideo(videos[currentVideo])
        }

        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)

        videoView.setOnPreparedListener() { mp ->
            mp.isLooping = true
        }

        playVideo(videos[0])
    }
}