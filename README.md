# Description

This is just very primitive Android app that I use to test video codec support on various embedded boards running Android. Outside of this narrow use case, the app's utility is negligible. You will also not learn about good coding
practices here, I'm afraid.

# Installation
    
1. Connect to the test board with ABD:

        adb connect <ip-addr>
        adb root

    Depending on the board, you may first have to enable ADB over TCP (for use a USB bridge, but I prefer TCP):

        setprop service.adb.tcp.port 5555 && stop adbd && start adbd
        
1. Open the cloned project in Android Studio and install it to the board. It will not play any vides on the first run, because the videos are not available yet.
    
1. Download the following test videos from [here](https://senkorasic.com/testmedia/):
    * [MP4/h.264](https://s3.amazonaws.com/senkorasic.com/test-media/video/caminandes-llamigos/caminandes_llamigos_1080p.mp4)
    * [MP4/h.265](https://s3.amazonaws.com/senkorasic.com/test-media/video/caminandes-llamigos/caminandes_llamigos_1080p_hevc.mp4)
    * [WEBM](https://s3.amazonaws.com/senkorasic.com/test-media/video/caminandes-llamigos/caminandes_llamigos_1080p.webm)

1. Push the files from your local download directory to the app's external storage directory:

        adb push <download-dir>/caminandes_llamigos_1080p* /storage/emulated/0/Android/data/ch.felfel.videotest/files/

1. Restart the app from Android studio. The video should start playing immediately, with some basic content information in the header. The currently selected video will loop forever, unless you use the "Next Video" button to change the video.

It should be self-evident how to modify the app to use different or more videos for testing of other codecs and/or resolutions.